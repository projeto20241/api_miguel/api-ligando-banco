const connect = require("../db/connect");

module.exports = class clienteController {
  static async createCliente(req, res) {
    const {
      cpf,
      nome,
      telefone,
      estado,
      cidade,
      bairro,
      logradouro,
      cep,
      numero,
      complemento,
      referencia,
    } = req.body;

    const queryInsert = `INSERT INTO cliente(cpf, nome, telefone, estado, cidade, bairro, logradouro, cep, numero, complemento, referencia) VALUES('${cpf}', '${nome}', '${telefone}', '${estado}', '${cidade}', '${bairro}', '${logradouro}', '${cep}', '${numero}', '${complemento}', '${referencia}')`;
    try {
      connect.query(queryInsert, function (err) {
        if (err) {
          console.log("Erro: ", err);
          res.status(500).json({ error: "Erro ao cadastrar" });
          return;
        }
        console.log("Usuário cadastrado");
        res.status(201).json({ message: "Usuário cadastrado" });
      });
    } catch (error) {
      console.log("Erro ao executar o cadastro", error);
      res.status(500).json({ erro: "Erro do servidor" });
    } // fim do bloco try catch
  }

  static async getAllClientes(req, res) {
    const selectClientes = `SELECT * FROM cliente`;
    try {
      connect.query(selectClientes, function (err, result) {
        if (err) {
          console.log("Erro: ", err);
          res.status(500).json({ error: "Erro ao buscar clientes" });
          return;
        }
        console.log("Clientes encontrados");
        res.status(201).json({ message: "Clientes encontrados", result });
      });
    } catch (error) {
      console.log("Erro ao executar a busca", error);
      res.status(500).json({ erro: "Erro do servidor" });
    } // fim do bloco try catch
  }

  //Exemplo Adriano
  static async getAllClientes2(req, res) {
    const selectClientes = `SELECT * FROM cliente`;
    try {
      connect.query(selectClientes, function (err, data) {
        if (err) {
          console.log("Erro: ", err);
          res.status(500).json({ error: "Erro ao buscar clientes" });
          return;
        }
        let clientes = data;
        console.log("Clientes encontrados");
        res.status(201).json({ message: 'Clientes encontrados: ', clientes})
      });
    } catch (error) {
      console.error("Erro ao executar a consulta: ", error);
      res.status(500).json({ error: "Erro do servidor"  })
    } // fim do bloco try catch
  }

  static async getAllClientes3(req, res){
    //Constantes para extração de parâmetros da consulta URL
    const { filtro, ordenacao, ordem } = req.query;
    //Query base(construção da consulta SQL base)
    let query = `SELECT * FROM cliente`;
    //Adicionando a claúsula WHERE, quando houver
    if (filtro) {
      //query = query + filtro; - está incorrecto
      //query = query + ` WHERE ${filtro}`;
      query += ` WHERE ${filtro}`;
    } 
    //Adicionar a cláusula order by, quando houver
    if(ordenacao) {
      query += ` ORDER BY ${ordenacao}`
      //Adicionar a oredm do order by
      if(ordem){
        query += ` ${ordem}`
      }
    }
    try {
      connect.query(query, function(err, result) {
        if (err) {
          console.log("Erro: ", err);
          res.status(500).json({ error: "Erro ao buscar clientes" });
          return;
        }
        console.log("Clientes encontrados");
        res.status(201).json({ message: 'Clientes encontrados: ', result})
      });
    } catch (error) {
      console.error("Erro ao executar a consulta: ", error);
      res.status(500).json({ error: "Erro do servidor"  })
    }
  }
};