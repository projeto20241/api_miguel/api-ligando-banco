const connect = require("../db/connect");

module.exports = class pizzariaController {
  static async listarPedidosPizzas(req, res) {
    const query = `
      SELECT
        pp.fk_id_pedido AS Pedido, 
        p.nome AS Pizza,
        pp.quantidade AS Qtde, 
        ROUND((pp.valor / pp.quantidade), 2) AS Unitário,
        pp.valor AS Total
      FROM
        pizza_pedido pp,
        pizza p
      WHERE
        pp.fk_id_pizza = p.id_pizza
      ORDER BY
        pp.fk_id_pedido;
    `;

    try {
      connect.query(query, function (err, result) {
        if (err) {
          console.log(err);
          return res.status(500).json({
            error: "Erro ao consultar pedidos de pizzas no banco de dados!!!",
          });
        }

        console.log("Consulta de pedidos de pizzas realizada com sucesso!!!");
        return res.status(200).json({ result });
      });
    } catch (error) {
      console.error(
        "Erro ao executar a consulta de pedidos de pizzas: ",
        error
      );
      return res.status(500).json({ error: "Erro interno do servidor!!!" });
    }
  }

  static async listarPedidosPizzasJoin(req, res) {
    const query = `
    select
	pp.fk_id_pedido as Pedido, p.nome as Pizza,
	pp.quantidade as Qtde, 
	round((pp.valor/pp.quantidade),2) as Unitário,
	pp.valor as Total
from
	pizza_pedido pp INNER JOIN pizza p
on
	pp.fk_id_pizza = p.id_pizza
order by pp.fk_id_pedido;
    `;

    try {
      connect.query(query, function (err, result) {
        if (err) {
          console.log(err);
          return res.status(500).json({
            error: "Erro ao consultar pedidos de pizzas no banco de dados!!!",
          });
        }

        console.log("Consulta de pedidos de pizzas realizada com sucesso!!!");
        return res.status(200).json({ result });
      });
    } catch (error) {
      console.error(
        "Erro ao executar a consulta de pedidos de pizzas: ",
        error
      );
      return res.status(500).json({ error: "Erro interno do servidor!!!" });
    }
  }
};
