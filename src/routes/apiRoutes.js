const router = require("express").Router();
const clienteController = require("../controller/clienteController");
const DBcontroller = require("../controller/DBcontroller");
const pizzariaController = require("../controller/pizzariaController");

//rota para consulta das tabelas
router.get("/name", DBcontroller.getName);
router.get("/descUser", DBcontroller.descUser);
router.get("/descArea", DBcontroller.descArea);
router.get("/descBooking", DBcontroller.descBooking);

//rota para cadastro de clientes
router.post("/postcliente/", clienteController.createCliente);
router.get("/cliente", clienteController.getAllClientes);
router.get("/cliente/2", clienteController.getAllClientes2);
router.get("/ cliente/3", clienteController.getAllClientes3);

//rota para listar as pizzas pedidas
router.get("/listarPedidosPizza/", pizzariaController.listarPedidosPizzas);
router.get("/listarPedidosPizzaJoin/", pizzariaController.listarPedidosPizzasJoin);

module.exports = router;
